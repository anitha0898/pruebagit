package ar.edu.unju.fi.demoTP4.main.git.model;

import org.springframework.stereotype.Component;

@Component
public class Compras {
	private int idComrpa;
	private Producto producto;
	private int cantidad;
 //constructores
	public Compras(int idComrpa, Producto producto, int cantidad) {
		super();
		this.idComrpa = idComrpa;
		this.producto = producto;
		this.cantidad = cantidad;
	}
	public Compras() {
		super();
	}
	public int getIdComrpa() {
		return idComrpa;
	}
	public void setIdComrpa(int idComrpa) {
		this.idComrpa = idComrpa;
	}
	public Producto getProducto() {
		return producto;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	
}
